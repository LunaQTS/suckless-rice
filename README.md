<img src="rice.png" width=100%>
<p align="center">Luna's suckless rice</p>

<br>

This is just a fork of hnhx's suckless rice (https://github.com/hnhx/rice) with minor modifications for my liking.

How to install:

```
git clone https://codeberg.org/lunaqts/suckless-rice.git
cd suckless-rice
make
sudo make install
chsh -s /bin/bash (if bash is not your default shell already)
```

The packages you will need: `bash xclip scrot dmenu xwallpaper picom ncurses hack (font)`
Please note that you must add `picom &` to your xinitrc file in order to have picom run on startup.
