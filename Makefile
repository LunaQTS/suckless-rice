all:
	cp -f ./bashrc ~/.bashrc
	cp -f ./Xresources ~/.Xresources
	cp -f ./xinitrc ~/.xinitrc
	cp -f ./picom.conf ~/.config/
	mkdir -p ~/.config/rice
	cp -f bg.jpg ~/.config/rice/
	xwallpaper --zoom ./bg.jpg

install:
	$(MAKE) -C suckless/st install
	$(MAKE) -C suckless/dwm install
	$(MAKE) -C suckless/slstatus install
	cp -f ./scripts/* /usr/local/bin/
