PATH=$PATH:~/.local/bin

PROMPT="%n:%F{blue}%~%f/ $ "

# Use below if lsd is installed.
# alias ls='lsd'
alias nf='neofetch'
alias vi='nvim'
alias vim='nvim'
alias c='clear'

# Use these based on a given distro.
# alias update='sudo pacman -Syu'
# alias update='doas apk update && doas apk upgrade'
# alias update='sudo apt update && sudo apt upgrade'

alias dl='yt-dlp'
alias yt='ytfzf'
alias sx='swallow sxiv'
alias mpv='swallow mpv'
alias mp='swallow mplayer'

# If using pywal for terminal color scheme
# (cat ~/.cache/wal/sequences &)

# If using starship prompt (replace bash with whatever shell I think)
# eval "$(starship init bash)"
